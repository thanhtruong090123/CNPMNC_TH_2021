import React from 'react'

export default function Header() {
    return (
        <div>
            <div className="row">
                <div className="col-3">
                    <h3>Thêm không gian cho gia đình và bạn bè</h3>
                    <br/>
                    <p>Rất nhiều lựa chọn hấp dẫn các căn hộ và biệt thự trên Traveloka</p>
                </div>
                <div className="col-3">
                    <img src="https://ik.imagekit.io/tvlk/image/imageResource/2019/07/01/1561969742861-54d523759500f217d6b7a094c3db0e43.jpeg?tr=q-75,w-298"/>
                    <br/>
                    <b>Biệt thự</b>
                    <p>17,000+ Biệt thự</p>
                </div>
                <div className="col-3">
                    <img src="https://ik.imagekit.io/tvlk/image/imageResource/2019/07/01/1561969736953-639cf0e0bacf7b1f0fbf05121ea147ac.jpeg?tr=q-75,w-298"/>
                    <br/>
                    <b>Căn hộ</b>
                    <p>25,000+ Căn hộ</p>
                </div>
            </div>
            <hr style={{width:'1000px',marginLeft:'-50px'}}/>
            <div className="row">
                <div className="col-3">
                    <h3>Thêm không gian cho gia đình và bạn bè</h3>
                    <br/>
                    <p>Rất nhiều lựa chọn hấp dẫn các căn hộ và biệt thự trên Traveloka</p>
                </div>
                <div className="col-3">
                    <img src="https://ik.imagekit.io/tvlk/image/imageResource/2019/07/01/1561969742861-54d523759500f217d6b7a094c3db0e43.jpeg?tr=q-75,w-298"/>
                    <br/>
                    <b>Biệt thự</b>
                    <p>17,000+ Biệt thự</p>
                </div>
                <div className="col-3">
                    <img src="https://ik.imagekit.io/tvlk/image/imageResource/2019/07/01/1561969736953-639cf0e0bacf7b1f0fbf05121ea147ac.jpeg?tr=q-75,w-298"/>
                    <br/>
                    <b>Căn hộ</b>
                    <p>25,000+ Căn hộ</p>
                </div>
            </div>
            <hr style={{width:'1000px',marginLeft:'-50px'}}/>
            <div className="row">
                <div className="col-3">
                    <h3>Thêm không gian cho gia đình và bạn bè</h3>
                    <br/>
                    <p>Rất nhiều lựa chọn hấp dẫn các căn hộ và biệt thự trên Traveloka</p>
                </div>
                <div className="col-3">
                    <img src="https://ik.imagekit.io/tvlk/image/imageResource/2019/07/01/1561969742861-54d523759500f217d6b7a094c3db0e43.jpeg?tr=q-75,w-298"/>
                    <br/>
                    <b>Biệt thự</b>
                    <p>17,000+ Biệt thự</p>
                </div>
                <div className="col-3">
                    <img src="https://ik.imagekit.io/tvlk/image/imageResource/2019/07/01/1561969736953-639cf0e0bacf7b1f0fbf05121ea147ac.jpeg?tr=q-75,w-298"/>
                    <br/>
                    <b>Căn hộ</b>
                    <p>25,000+ Căn hộ</p>
                </div>
            </div>  
        </div>
    )
}
