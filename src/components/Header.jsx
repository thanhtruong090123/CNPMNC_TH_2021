import React from 'react'
import '../css/Header.css'

export default function Header() {
    return (
        <div className='body'>
                <span className='name'><a href="/">Traveloka</a></span>
                <span className='ul'>
                    <span className='li'>
                        <span><a href="/">Khuyến mãi</a></span>
                        <span><a href="/">Hộp thư của tôi</a></span>
                        <span><a href="/">Đã lưu</a></span>
                        <span><a href="/">Đặt chỗ của tôi</a></span>
                        <span><a href="/">VNĐ</a></span>
                    </span>
                </span>
              
        </div>
    )
}
